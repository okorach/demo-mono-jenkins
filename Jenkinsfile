buildDir = "build"
pylintReport = "${buildDir}/pylint-report.out"
banditReport = "${buildDir}/bandit-report.json"
flake8Report = "${buildDir}/flake8-report.out"
coverageReport = "${buildDir}/coverage.xml"

coverageTool = 'coverage'
pylintTool = 'pylint'
flake8Tool = 'flake8'
banditTool = 'bandit'

pipeline {
  agent any
  stages {
    stage('Code Checkout') {
      steps {
        updateGitlabCommitStatus name: 'jenkins', state: 'pending'
        checkout([$class: 'GitSCM', branches: [[name: '**']], extensions: [[$class: 'CloneOption', noTags: false, reference: '', shallow: false]], userRemoteConfigs: [[credentialsId: 'GitLabPAT2', url: 'https://gitlab.com/okorach/demo-mono-jenkins']]])
        updateGitlabCommitStatus name: 'jenkins', state: 'running'
      }
    }
    stage('Run tests') {
      steps {
        script {
          echo "Run unit tests for coverage"
          sh "cd comp-cli; ${coverageTool} run -m pytest || echo echo Tests failed, continuing anyway"
          echo "Generate XML report"
          sh "cd comp-cli; ${coverageTool} xml -o ${coverageReport} || echo Tests failed, continuing anyway"
          updateGitlabCommitStatus name: 'jenkins', state: 'running'
        }
      }
    }
    stage('Run 3rd party linters') {
     steps {
        script {
          // sh "cd comp-cli; ${pylintTool} *.py */*.py -r n --msg-template=\"{path}:{line}: [{msg_id}({symbol}), {obj}] {msg}\" > ${pylintReport}"
          // sh "cd comp-cli; ${flake8Tool} --ignore=W503,E128,C901,W504,E302,E265,E741,W291,W293,W391 --max-line-length=150 . > ${flake8Report}"
          // sh "cd comp-cli; ${banditTool} -f json --skip B311,B303 -r . -x .vscode,./testpytest,./testunittest > ${banditReport}"
          sh "cd comp-cli; ./run_linters.sh"
          updateGitlabCommitStatus name: 'jenkins', state: 'running'
        }
      }
    }
    stage('SonarQube LATEST analysis - CLI') {
      steps {
        withSonarQubeEnv('SQ Latest') {
          script {
            def scannerHome = tool 'SonarScanner';
            sh "cd comp-cli; ${scannerHome}/bin/sonar-scanner"
            updateGitlabCommitStatus name: 'jenkins', state: 'running'
          }
        }
      }
    }
    stage("SonarQube LATEST Quality Gate - CLI") {
      steps {
        timeout(time: 5, unit: 'MINUTES') {
          script {
            def qg = waitForQualityGate()
            if (qg.status != 'OK') {
              updateGitlabCommitStatus name: 'jenkins', state: 'failed'
              error("CLI component quality gate failed: ${qg.status}")
            }
            sh 'rm -f comp-cli/.scannerwork/report-task.txt'
          }
        }
      }
    }
    stage('SonarQube LATEST analysis - Maven') {
      steps {
        withSonarQubeEnv('SQ Latest') {
          script {
            sh 'cd comp-maven; mvn clean org.jacoco:jacoco-maven-plugin:prepare-agent install org.jacoco:jacoco-maven-plugin:report sonar:sonar'
            updateGitlabCommitStatus name: 'jenkins', state: 'running'
          }
        }
      }
    }
    stage("SonarQube LATEST Quality Gate - Maven") {
      steps {
        timeout(time: 5, unit: 'MINUTES') {
          script {
            def qg = waitForQualityGate()
            if (qg.status != 'OK') {
              updateGitlabCommitStatus name: 'jenkins', state: 'failed'
              error("Maven component quality gate failed: ${qg.status}")
            }
            sh 'rm -f comp-maven/target/sonar/report-task.txt'
          }
        }
      }
    }
    stage('SonarQube LATEST analysis - Gradle') {
      steps {
        withSonarQubeEnv('SQ Latest') {
          script {
            sh 'cd comp-gradle; ./gradlew jacocoTestReport sonarqube'
            updateGitlabCommitStatus name: 'jenkins', state: 'running'
          }
        }
      }
    }
    stage("SonarQube LATEST Quality Gate - Gradle") {
      steps {
        timeout(time: 5, unit: 'MINUTES') {
          script {
            def qg = waitForQualityGate()
            if (qg.status != 'OK') {
              updateGitlabCommitStatus name: 'jenkins', state: 'failed'
              error("Gradle component quality gate failed: ${qg.status}")
            }
            sh 'rm -f comp-gradle/build/sonar/report-task.txt'
          }
        }
      }
    }
    stage('SonarQube LATEST analysis - .Net') {
      steps {
        script {
          def dotnetScannerHome = tool 'Scanner for .Net Core'
          withSonarQubeEnv('SQ Latest') {
            sh "cd comp-dotnet; /usr/local/share/dotnet/dotnet ${dotnetScannerHome}/SonarScanner.MSBuild.dll begin /k:\"demo:gitlab-mono-jenkins-dotnet\" /n:\"GitLab / Jenkins / monorepo .Net Core\"" 
            sh "cd comp-dotnet; /usr/local/share/dotnet/dotnet build"
            updateGitlabCommitStatus name: 'jenkins', state: 'running'
            sh "cd comp-dotnet; /usr/local/share/dotnet/dotnet ${dotnetScannerHome}/SonarScanner.MSBuild.dll end"
            updateGitlabCommitStatus name: 'jenkins', state: 'running'
          }
        }
      }
    }
    stage("SonarQube LATEST Quality Gate - .Net") {
      steps {
        timeout(time: 5, unit: 'MINUTES') {
          script {
            def qg = waitForQualityGate()
            if (qg.status != 'OK') {
              updateGitlabCommitStatus name: 'jenkins', state: 'failed'
              echo ".Net component quality gate failed: ${qg.status}, proceeding anyway"
            }
            sh 'rm -f comp-dotnet/.sonarqube/out/.sonar/report-task.txt'
          }
        }
      }
    }
  }
}
